# Synthpendium

## Development
### Requirements
* [Go](http://golang.org) 1.14+
* [Mage](https://magefile.org)
* [go-bindata](https://github.com/go-bindata/go-bindata)

### Build
```
# Produce bin/server
mage
```

### Run Locally
```
mage run
```

### Deploy to Google Cloud Run
To deploy the service to Google Cloud Run, execute the following command:

```
mage deploy
```

### Database
We use Google Cloud SQL to provide us with a PostgreSQL database. The database schema is maintained
via migrations in the db/migrations directory. We use
[golang-migrate/migrate](https://github.com/golang-migrate/migrate) for handling migrations.
On startup of the server, it uses the migrate library to apply any missing migrations to the
Cloud SQL database.
