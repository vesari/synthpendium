package handlers

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/vesari/synthpendium/data"

	"github.com/gorilla/mux"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type HSynth struct {
	l     *log.Logger
	pgURI string
}

func NewHSynth(l *log.Logger, pgURI string) *HSynth {
	return &HSynth{l, pgURI}
}

func (h *HSynth) ListAllSynths(rw http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	var synths []data.Synthesizer

	query := `SELECT 
	*
	FROM 
	synthesizer ;`

	rows, err := dbpool.Query(ctx, query)
	if err != nil {
		log.Printf("Query failed: %v", err)
		rw.WriteHeader(500)
		return
	}

	defer rows.Close()

	for rows.Next() {
		var synth data.Synthesizer
		if err := rows.Scan(&synth.ID, &synth.Brand, &synth.Model, &synth.Synthesis, &synth.ChassisType, &synth.OctaveQuantity, &synth.KeysSize, &synth.UnitQuantity); err != nil {
			log.Printf("Rows scan failed: %v", err)
		}
		synths = append(synths, synth)
	}
	if err := rows.Err(); err != nil {
		log.Printf("Rows failed: %v", err)
	}

	err = json.NewEncoder(rw).Encode(synths)

	if err != nil {
		log.Printf("Failed to encode synthesizers: %v", err)
		rw.WriteHeader(400)
		return
	}

	log.Printf("Successfully listed synths")
}

func (h *HSynth) ListSynthsByStudioName(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //passing the request to vars
	name := vars["name"]

	log.Printf("Getting synths by studio name %q", name)

	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	type model struct {
		Model string `json:"model"`
		Brand string `json:"brand"`
	}
	var models []model

	query := `SELECT 
	brand, model 
	FROM 
	synthesizer
	WHERE id IN
	(SELECT fk_synthesizer_id
	 FROM synth_studio_association
	INNER JOIN studio ON studio.id = synth_studio_association.fk_studio_id
	 WHERE name = $1
	);`

	rows, err := dbpool.Query(ctx, query, name)
	if err != nil {
		log.Printf("Query failed: %v", err)
		rw.WriteHeader(500)
		return
	}

	defer rows.Close()

	for rows.Next() {
		log.Printf("Adding synthesizer")
		var m model
		if err := rows.Scan(&m.Brand, &m.Model); err != nil {
			log.Printf("Rows scan failed: %v", err)
		}
		models = append(models, m)
	}
	if err := rows.Err(); err != nil {
		log.Printf("Rows failed: %v", err)
		rw.WriteHeader(500)
		return
	}

	err = json.NewEncoder(rw).Encode(models)
	if err != nil {
		log.Printf("Failed to encode synthesizers: %v", err)
		rw.WriteHeader(400)
		return
	}

	log.Printf("Successfully listed synths by studio name")
}

func (h *HSynth) GetSynthByModel(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) //passing the request to vars
	model := vars["model"]

	log.Printf("Getting synth by model %q", model)

	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	var synth data.Synthesizer

	query := `SELECT id, brand, model, synthesis, chassis_type, octave_quantity, keys_size, unit_quantity FROM synthesizer
WHERE model = $1`

	row := dbpool.QueryRow(ctx, query, model)
	if err := row.Scan(&synth.ID, &synth.Brand, &synth.Model, &synth.Synthesis, &synth.ChassisType, &synth.OctaveQuantity, &synth.KeysSize, &synth.UnitQuantity); err != nil {
		if err == pgx.ErrNoRows {
			log.Printf("Synth with name %q not found", model)
			http.Error(rw, "Synth not found", http.StatusNotFound)
		} else {
			log.Printf("Row scan failed: %v", err)
			http.Error(rw, "Internal server error", http.StatusInternalServerError)
		}
		return
	}

	err = json.NewEncoder(rw).Encode(synth)
	if err != nil {
		log.Printf("Failed to encode synthesizer: %s", err)
		http.Error(rw, "Internal server error", http.StatusInternalServerError)
		return
	}
	log.Printf("Successfully fetched synth by model")
}

func (h *HSynth) GetSynthsByBrand(rw http.ResponseWriter, r *http.Request) {
	log.Printf("Getting by brand")
	vars := mux.Vars(r) //passing the request to vars
	brand := vars["brand"]

	log.Printf("Getting synths by brand %q", brand)

	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	var synths []data.Synthesizer

	query := `SELECT id, brand, model, synthesis, chassis_type, octave_quantity, keys_size, unit_quantity FROM synthesizer
		WHERE brand = $1`

	rows, err := dbpool.Query(ctx, query, brand)
	if err != nil {
		log.Printf("Query failed: %v", err)
		rw.WriteHeader(500)
		return
	}

	defer rows.Close()

	for rows.Next() {
		var synth data.Synthesizer
		if err := rows.Scan(&synth.ID, &synth.Brand, &synth.Model, &synth.Synthesis, &synth.ChassisType, &synth.OctaveQuantity, &synth.KeysSize, &synth.UnitQuantity); err != nil {
			log.Printf("Rows scan failed: %v", err)
		}
		synths = append(synths, synth)
	}
	if err := rows.Err(); err != nil {
		log.Printf("Rows failed: %v", err)
	}

	err = json.NewEncoder(rw).Encode(synths)

	if err != nil {
		log.Printf("Failed to encode synthesizers: %v", err)
		rw.WriteHeader(400)
		return
	}

	log.Printf("Successfully listed synths by brand")
}

func (h *HSynth) AddSynths(rw http.ResponseWriter, r *http.Request) {
	log.Printf("Handling request to add synths")
	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	var synths []data.Synthesizer
	err = json.NewDecoder(r.Body).Decode(&synths)
	if err != nil {
		log.Printf("Failed to decode synthesizers: %v", err)
		rw.WriteHeader(400)
		return
	}

	log.Printf("Adding %d synth(s)", len(synths))

	for _, synth := range synths {
		query := `INSERT INTO synthesizer(brand, model, synthesis, chassis_type, octave_quantity, keys_size, unit_quantity) VALUES ($1, $2, $3, $4, $5, $6, $7)`
		_, err = dbpool.Exec(ctx, query, synth.Brand, synth.Model, synth.Synthesis, synth.ChassisType, synth.OctaveQuantity, synth.KeysSize, synth.UnitQuantity)
		if err != nil {
			log.Printf("Adding synths failed: %v", err)
			rw.WriteHeader(500)
			return
		}
	}

	log.Printf("Successfully added synths")
}

func (h *HSynth) DeleteSynths(rw http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	dbpool, err := pgxpool.Connect(ctx, h.pgURI)
	if err != nil {
		log.Printf("Unable to connect to database: %v", err)
		rw.WriteHeader(500)
		return
	}
	defer dbpool.Close()

	query := `DELETE FROM synthesizer`

	_, err = dbpool.Exec(ctx, query)
	if err != nil {
		log.Printf("Exec failed: %v", err)
		rw.WriteHeader(500)
		return
	}

	log.Printf("Successfully cleared synthesizer table")
}
