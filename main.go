package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	migrate "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	"github.com/joho/godotenv"

	"gitlab.com/vesari/synthpendium/handlers"
	"gitlab.com/vesari/synthpendium/migrations"

	"github.com/gorilla/mux"
)

func main() {
	logger := log.New(os.Stdout, "main", log.LstdFlags)

	err := godotenv.Load()
	if err != nil {
		logger.Printf("Can't find .env file")
	}

	postgresUser := os.Getenv("POSTGRES_USER")
	if postgresUser == "" {
		log.Fatal("$POSTGRES_USER not set")
	}
	postgresPassword := os.Getenv("POSTGRES_PASSWORD")
	if postgresPassword == "" {
		log.Fatal("$POSTGRES_PASSWORD not set")
	}

	dbName := "synthpendium"
	var postgresURI string
	instanceConnName := os.Getenv("INSTANCE_CONNECTION_NAME")
	if instanceConnName != "" {
		// We're on Cloud Run
		logger.Printf("Connecting to Cloud SQL Proxy")
		socket := fmt.Sprintf("/cloudsql/%s", instanceConnName)
		// The Unix socket must be provided as query parameter "host"
		// It's also required to disable SSL mode when using the Cloud SQL proxy
		postgresURI = fmt.Sprintf("postgres://%s:%s@/%s?host=%s&sslmode=disable", postgresUser,
			postgresPassword, dbName, socket)
	} else {
		host := os.Getenv("POSTGRES_HOST")
		if host == "" {
			log.Fatal("$POSTGRES_HOST not set")
		}
		port := os.Getenv("POSTGRES_PORT")
		if port == "" {
			log.Fatal("$POSTGRES_PORT not set")
		}
		postgresURI = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", postgresUser,
			postgresPassword, host, port, dbName)
		logger.Printf("Connecting to Postgres at: %s:%s", host, port)
	}

	migAssets := migrations.AssetNames()
	s := bindata.Resource(migAssets, func(name string) ([]byte, error) {
		a, err := migrations.Asset(name)
		if err != nil {
			logger.Printf("Failed to get migration asset %q", name)
			return nil, err
		}
		return a, nil
	})
	d, err := bindata.WithInstance(s)
	if err != nil {
		log.Fatal(err.Error())
	}

	logger.Printf("Creating migrations object, URI: %q", postgresURI)
	m, err := migrate.NewWithSourceInstance("go-bindata", d, postgresURI)
	if err != nil {
		logger.Fatalf("Creating migrations object failed: %s", err)
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		logger.Fatalf("Migrating database failed: %s", err)
	}

	hhs := handlers.NewHSynth(logger, postgresURI)
	sm := mux.NewRouter()

	getRouter := sm.Methods(http.MethodGet).Subrouter()
	getRouter.HandleFunc("/synths", hhs.ListAllSynths)
	getRouter.HandleFunc(`/synths/studio/name/{name:[-\0-9A-Za-z]+}`, hhs.ListSynthsByStudioName)
	getRouter.HandleFunc(`/synths/model/{model:[-\0-9A-Za-z]+}`, hhs.GetSynthByModel)
	getRouter.HandleFunc(`/synths/brand/{brand:[-\0-9A-Za-z]+}`, hhs.GetSynthsByBrand)

	postRouter := sm.Methods(http.MethodPost).Subrouter()
	postRouter.HandleFunc("/synths", hhs.AddSynths)

	deleteRouter := sm.Methods(http.MethodDelete).Subrouter()
	deleteRouter.HandleFunc("/synths", hhs.DeleteSynths)

	logger.Printf("Listening on port 8080")
	if err := http.ListenAndServe(":8080", sm); err != nil {
		log.Fatal(err.Error())
	}
}
