FROM golang:1.17.7-alpine3.15 AS go-builder

WORKDIR /app

COPY . .

RUN go get -u github.com/go-bindata/go-bindata/...
RUN go generate ./...
RUN go build -o bin/synth

FROM alpine:3.15.0

WORKDIR /app

COPY --from=go-builder /app/bin/synth bin/synth

ENTRYPOINT bin/synth
