package data

type Synthesizer struct {
	ID             int      `json:"id"`
	Brand          string   `json:"brand"`
	Model          string   `json:"model"`
	Synthesis      []string `json:"synthesis"`
	ChassisType    string   `json:"chassis_type"` // TODO: ENUM for Keyboard, Rack, Desktop...
	OctaveQuantity int      `json:"octave_quantity,omitempty"`
	KeysSize       string   `json:"keys_size,omitempty"`
	UnitQuantity   int      `json:"unit_quantity,omitempty"`
}

type Oscillator struct {
	ID               int      `json:"id"`
	SynthesizerID    int      `json:"fk_synthesizer_id"`
	Name             string   `json:"name"`
	Type             string   `json:"type"`
	Model            string   `json:"model"`
	WaveShapes       []string `json:"wave_shapes"`
	FreqRangeBeginHz float64  `json:"freq_range_begin_hz"`
	FreqRangeEndMgHz float64  `json:"freq_range_end_mghz"`
}

type FilterBank struct {
	ID         int      `json:"id"`
	Name       string   `json:"name"`
	Type       string   `json:"type"`
	Model      string   `json:"model"`
	WaveShapes []string `json:"wave_shapes"`
	Envelopes  []string `json:"envelopes"`
}

type Studio struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	City    string `json:"city"`
	Country string `json:"country"`
	EMail   string `json:"e_mail"`
}
