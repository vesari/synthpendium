package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/vesari/synthpendium/data"
)

// ListAllSynths
// GetSynthByName
// TODO: GetSynthByStudioName

func TestListAllSynths(t *testing.T) {
	var c http.Client

	err := godotenv.Load()
	require.NoError(t, err)
	srvURL := os.Getenv("SERVICE_URL")
	require.NotEmpty(t, srvURL)
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/synths", srvURL), nil)
	require.NoError(t, err)
	resp, err := c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Deleting synthesizers from %s fails: %d", srvURL, resp.StatusCode))

	body, err := json.Marshal(synthCatalogue)
	require.NoError(t, err)
	reader := bytes.NewReader(body)
	req, err = http.NewRequest("POST", fmt.Sprintf("%s/synths", srvURL), reader)
	require.NoError(t, err)
	resp, err = c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Adding synthesizers from %s fails: %s", srvURL, resp.Status))

	req, err = http.NewRequest("GET", fmt.Sprintf("%s/synths", srvURL), nil)
	require.NoError(t, err)
	resp, err = c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Listing synths from %s fails: %s", srvURL, resp.Status))
	var gotSynths []data.Synthesizer
	err = json.NewDecoder(resp.Body).Decode(&gotSynths)
	require.NoError(t, err)
	diff := cmp.Diff(synthCatalogue, gotSynths, cmp.FilterPath(func(p cmp.Path) bool {
		return p.String() == "ID"
	}, cmp.Ignore()))
	assert.Empty(t, diff)
}

func TestGetSynthByModel(t *testing.T) {
	var c http.Client

	err := godotenv.Load()
	require.NoError(t, err)
	srvURL := os.Getenv("SERVICE_URL")
	require.NotEmpty(t, srvURL)
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/synths", srvURL), nil)
	require.NoError(t, err)
	resp, err := c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Deleting synthesizers from %s fails: %d", srvURL, resp.StatusCode))

	body, err := json.Marshal(synthCatalogue)
	require.NoError(t, err)
	reader := bytes.NewReader(body)
	req, err = http.NewRequest("POST", fmt.Sprintf("%s/synths", srvURL), reader)
	require.NoError(t, err)
	resp, err = c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Adding synthesizers from %s fails: %s", srvURL, resp.Status))

	req, err = http.NewRequest("GET", fmt.Sprintf("%s/synths/model/TX81Z", srvURL), nil)
	require.NoError(t, err)
	resp, err = c.Do(req)
	require.NoError(t, err)
	require.NotNil(t, resp)
	require.Equal(t, 2, resp.StatusCode/100, fmt.Sprintf("Fetching synth from %s fails: %s", srvURL, resp.Status))
	var gotSynth data.Synthesizer
	err = json.NewDecoder(resp.Body).Decode(&gotSynth)
	require.NoError(t, err)
	diff := cmp.Diff(data.Synthesizer{
		Brand:          "Yamaha",
		Model:          "TX81Z",
		Synthesis:      []string{"FM"},
		ChassisType:    "rack",
		OctaveQuantity: 0,
		KeysSize:       "",
		UnitQuantity:   1,
	}, gotSynth, cmp.FilterPath(func(p cmp.Path) bool {
		return p.String() == "ID"
	}, cmp.Ignore()))
	assert.Empty(t, diff)
}

var synthCatalogue = []data.Synthesizer{
	{
		Brand:          "Sequential Circuits",
		Model:          "Pro-One",
		Synthesis:      []string{"Analogue"},
		ChassisType:    "keyboard",
		OctaveQuantity: 2,
		KeysSize:       "regular",
		UnitQuantity:   0,
	},

	{
		Brand:          "Yamaha",
		Model:          "TX81Z",
		Synthesis:      []string{"FM"},
		ChassisType:    "rack",
		OctaveQuantity: 0,
		KeysSize:       "",
		UnitQuantity:   1,
	},
}
