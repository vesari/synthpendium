CREATE TABLE IF NOT EXISTS studio(
   id SERIAL PRIMARY KEY,
   name TEXT NOT NULL,
   city TEXT NOT NULL,
   country TEXT NOT NULL,
   e_mail TEXT
);