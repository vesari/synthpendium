CREATE TABLE IF NOT EXISTS synthesizer(
   id BIGSERIAL PRIMARY KEY,
   brand TEXT NOT NULL,
   model TEXT NOT NULL,
   synthesis TEXT[],
   chassis_type TEXT NOT NULL,
   octave_quantity SMALLINT,
   keys_size TEXT,
   unit_quantity SMALLINT
);
