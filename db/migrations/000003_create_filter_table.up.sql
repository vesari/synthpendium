CREATE TABLE IF NOT EXISTS filter_bank(
  id SERIAL PRIMARY KEY,
  fk_synthesizer_id BIGSERIAL,
  name TEXT,
  type TEXT,
  model TEXT,
  dB NUMERIC,
  wave_shapes TEXT [],
	envelopes TEXT [],
  FOREIGN KEY (fk_synthesizer_id) REFERENCES synthesizer(id) ON DELETE CASCADE
);