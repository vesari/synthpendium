CREATE TABLE IF NOT EXISTS synth_studio_association(
id SERIAL,
fk_synthesizer_id BIGSERIAL,
fk_studio_id SERIAL,
PRIMARY KEY (id),
FOREIGN KEY (fk_synthesizer_id) REFERENCES synthesizer(id) ON DELETE CASCADE,
FOREIGN KEY (fk_studio_id) REFERENCES studio(id) ON DELETE CASCADE
);