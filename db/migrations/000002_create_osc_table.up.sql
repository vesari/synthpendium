CREATE TABLE IF NOT EXISTS oscillator(
  id SERIAL PRIMARY KEY,
  fk_synthesizer_id BIGSERIAL,
  name TEXT,
  type TEXT,
  model TEXT,
  freq_range_begin_Hz NUMERIC,
  freq_range_end_MgHz NUMERIC,
  wave_shapes TEXT [],
  FOREIGN KEY (fk_synthesizer_id) REFERENCES synthesizer(id) ON DELETE CASCADE
);