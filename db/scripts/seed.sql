TRUNCATE TABLE synthesizer RESTART IDENTITY CASCADE;

INSERT INTO synthesizer 
(brand, model, synthesis, chassis_type, octave_quantity, keys_size, unit_quantity)
VALUES
('Roland', 'JD-Xi', '{"analogue", "digital"}', 'keyboard', 4, 'mini', 0),
('Roland', 'Alpha Juno 1', '{"analogue"}', 'keyboard', 4, 'regular', 0),
('Novation', 'K-Station', '{"analogue modelling"}', 'keyboard', 2, 'regular', 0),
('Roland', 'Gaia SH-01', '{"virtual analogue"}', 'keyboard', 4, 'regular', 0),
('Sequential Circuits Inc.', 'Pro-One', '{"analogue"}', 'keyboard', 3, 'regular', 0);

TRUNCATE TABLE studio RESTART IDENTITY CASCADE;

INSERT INTO studio
(name, city, country, e_mail)
VALUES
('Petite Madeleine','Asker','NO','ptitemad@me.com'),
('The Memory Room', 'Treviso', 'IT', 'tmroom@me.com');

INSERT INTO synth_studio_association 
(fk_synthesizer_id, fk_studio_id)
VALUES
(1,1), (1,2), (2,1), (3,1), (4,1), (4,2), (5,1);