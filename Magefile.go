//+build mage

package main

import (
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

func Generate() error {
	return sh.RunV("go", "generate", "./...")
}

func Build() error {
	mg.Deps(Generate)
	return sh.RunV("go", "build", "-o", "bin/server", ".")
}

func Run() error {
	mg.Deps(Generate)
	return sh.RunV("go", "run", "main.go")
}

func Docker() error {
	return sh.RunV("docker", "build", "-t", "eu.gcr.io/curious-sandbox-240819/synth", ".")
}

func Deploy() error {
	mg.Deps(Generate)
	return sh.RunV("gcloud", "run", "deploy", "--source", ".", "synthpendium")
}

var Default = Build
